<?php
/**
 * Класс одиночка по работе с БД
 */

class DB {
	private static $db;
	private static $instance;

	protected function __construct() {}
	protected function __clone() {}
	public function __wakeup() {}

	/**
	 * создание объекта класса
	 * @return object объект этого класса
	 */
	public static function getInstance() {
		if (!isset(static::$instance)) {
			static::$instance = new static;

			static::$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_BD);
			static::$db->set_charset("utf8");
		}

		return static::$instance;
	}

	/**
	 * обработка строки для выполнения запроса
	 * @param string $text строка для фильтрации
	 * @return mixed подготоволенная строка для записа
	 */
	public static function mres($text) {
		return static::$db->real_escape_string($text);
	}

	/**
	 * выборка пользователя по логину
	 *
	 * @param string $login логин пользователя
	 * @return array массив пользователя
	 */
	public static function getUserByLogin($login) {
		$q = static::$db->query('select * from users where login="'.static::mres($login).'"');
		if ($q->num_rows > 0) {
			return $q->fetch_assoc();
		}

		return [];
	}

	/**
	 * выборка пользователя по хэшу
	 *
	 * @param string $hash хэш пользователя
	 * @return array массив пользователя
	 */
	public static function getUserByHash($hash) {
		$q = static::$db->query('select * from users where hash="'.static::mres($hash).'"');
		if ($q->num_rows > 0) {
			return $q->fetch_assoc();
		}

		return [];
	}

	/**
	 * выборка поставщиков
	 *
	 * @return array массив поставщиков
	 */
	public static function getProviders($name = '') {
		$result = [];

		$sql = 'select * from providers ';
		$sql .= ' where (del=0)';
		$sql .= ($name != '') ? ' and (name like "%'.static::mres($name).'%")' : '';

		$q = static::$db->query($sql);
		if ($q->num_rows > 0) {
			while ($arr = $q->fetch_assoc()) {
				$result[$arr['id']] = $arr;
			}
		}

		return $result;
	}

	/**
	 * выборка продуктов
	 *
	 * @return array массив продуктов
	 */
	public static function getProducts($category = '', $name = '') {
		$result = [];

		$sql = 'select product_categories.name as category, units.name as unit, products.* from products';
		$sql .= ' inner join product_categories on product_categories.id=products.id_category';
		$sql .= ' inner join units on units.id=products.id_unit';
		$sql .= ' where (products.del=0)';
		$sql .= ($category != '') ? ' and (product_categories.name like "%'.static::mres($category).'%")' : '';
		$sql .= ($name != '') ? ' and (products.name like "%'.static::mres($name).'%")' : '';
		$sql .= ' order by category, name';

		$q = static::$db->query($sql);
		if ($q->num_rows > 0) {
			while ($arr = $q->fetch_assoc()) {
				$result[$arr['id']] = $arr;
			}
		}

		return $result;
	}

	/**
	 * выборка связей продуктов и поставщиков
	 *
	 * @return array массив связей продуктов и поставщиков
	 */
	public static function getLinksProductProvider() {
		$result = [];

		$q = static::$db->query('select * from product_providers');
		if ($q->num_rows > 0) {
			while ($arr = $q->fetch_assoc()) {
				$result[$arr['id_product']][$arr['id_provider']] = $arr;
			}
		}

		return $result;
	}

	/**
	 * обновление времени входа пользователя
	 *
	 * @param integer $id id пользователя
	 * @param string  $date дата в формате Y-m-d H:i:s
	 * @return boolean 0 - время не обновилось, 1 - время обновилось
	 */
	public static function setUserDateEnter($id, $date) {
		$q = static::$db->query('update users set date_enter="'.static::mres($date).'" where id="'.static::mres($id).'"');
		if ($q->affected_rows > 0) {
			return true;
		}

		return false;
	}

	/**
	 * обновление хэша пользователя
	 *
	 * @param integer $id id пользователя
	 * @param string  $hash хэш пользователя
	 * @return boolean 0 - хэш не обновился, 1 - хэш обновился
	 */
	public static function setUserHash($id, $hash) {
		$q = static::$db->query('update users set hash="'.static::mres($hash).'" where id="'.static::mres($id).'"');
		if ($q->affected_rows > 0) {
			return true;
		}

		return false;
	}
}