<?php

/**
 * Класс по поиску товаров и поставщиков
 */

class Search {
	/**
	 * выборка всех товаров и их поставщиков
	 *
	 * @return array массив товаров
	 */
	public function getAll() {
		DB::getInstance();

		$result = [];
		$nom = 1;

		$providers = DB::getProviders();
		$products = DB::getProducts();
		$link = DB::getLinksProductProvider();

		if ( (count($providers) > 0) && (count($products) > 0) && (count($link) > 0) ) {
			foreach ($products as $key => $value) {
				$product = [
					'nom' => $nom,
					'id' => $value['id'],
					'category' => $value['category'],
					'name' => $value['name'],
					'unit' => $value['unit'],
					'providers' => [],
				];

				if (isset($link[$value['id']])) {
					foreach ($link[$value['id']] as $k => $v) {
						if (isset($providers[$v['id_provider']])) {
							$product['providers'][] = [
								'id' => $v['id_provider'],
								'name' => $providers[$v['id_provider']]['name'],
								'price' => $v['price'],
							];
						}
					}
				}

				$product['id'] .= '_'.count($product['providers']);

				$result[] = $product;
				$nom++;
			}
		}

		return $result;
	}

	/**
	 * поиск товаров
	 *
	 * @param string $category название категории
	 * @param string $name название продукта
	 * @param string $provider название поставщика
	 * @return array массив товаров
	 */
	public function getSearch($category = '', $name = '', $provider = '') {
		DB::getInstance();

		$result = [];
		$nom = 1;

		$providers = DB::getProviders($provider);
		$products = DB::getProducts($category, $name);
		$link = DB::getLinksProductProvider();

		if ( (count($providers) > 0) && (count($products) > 0) && (count($link) > 0) ) {
			foreach ($products as $key => $value) {
				if (isset($link[$value['id']])) {
					$product = [
						'nom' => $nom,
						'id' => $value['id'],
						'category' => $value['category'],
						'name' => $value['name'],
						'unit' => $value['unit'],
						'providers' => [],
					];
	
					foreach ($link[$value['id']] as $k => $v) {
						if (isset($providers[$v['id_provider']])) {
							$product['providers'][] = [
								'id' => $v['id_provider'],
								'name' => $providers[$v['id_provider']]['name'],
								'price' => $v['price'],
							];
						}
					}

					if (count($product['providers']) == 0) {
						continue;
					}

					$product['id'] .= '_'.count($product['providers']);

					$result[] = $product;
					$nom++;
				}
			}
		}

		return $result;
	}
}

?>