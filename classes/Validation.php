<?php

/**
 * Класс для валидации данных
 */

class Validation {
	/**
	 * проверка строки на соответствие логину
	 *
	 * @param string $text строка для проверки
	 * @return boolean 0 - проверка не пройдена, 1 - проверка пройдена
	 */
	public static function stringEnglish($text) {
		if ( ($text != '') && (mb_strpos("~[^a-zA-Z0-9\-\_]~", $text) == 0) ) {
			return true;
		}

		return false;
	}
}

?>