<?php
/**
 * Класс по обработке AJAX запросов
 */

class Ajax {
	public $result = [];
	public $action;

	/**
	 * проверка на запрос
	 *
	 * @return boolean 0 - не разрешён, 1 - разрешён
	 */
	public function checkRequest() {
		if (isset($_POST['authorization'])) {
			$this->action = 'authorization_'.$_POST['authorization'];
			
			return true;
		}

		if (isset($_POST['products'])) {
			$this->action = 'products_'.$_POST['products'];
			
			return true;
		}

		return false;
	}

	/**
	 * установка ответа по авторизации
	 */
	public function setAuthorizationResponse() {
		$this->result = [
			'type' => 'error',
			'errors' => [
				'login' => '',
				'password' => '',
			]
		];
	}

	/**
	 * установка ответа только с типом
	 */
	public function setTypeResponse() {
		$this->result = [
			'type' => 'error',
		];
	}

	/**
	 * установка ответа по выводу продуктов
	 */
	public function setProductsResponse() {
		$this->result = [
			'type' => 'error',
			'products' => [],
		];
	}

	/**
	 * проверка на ввод логина
	 *
	 * @return boolean|string строка логина или ошибка
	 */
	public function checkLogin() {
		if (isset($_POST['login'])) {
			if (Validation::stringEnglish($_POST['login'])) {
				return $_POST['login'];
			} else {
				$this->result['errors']['login'] = 'Поле должно быть заполнено правильно';
			}
		}

		return false;
	}

	/**
	 * проверка на ввод пароля
	 *
	 * @return boolean|string строка пароля или ошибка
	 */
	public function checkPassword() {
		if (isset($_POST['password'])) {
			if (Validation::stringEnglish($_POST['password'])) {
				return $_POST['password'];
			} else {
				$this->result['errors']['password'] = 'Поле должно быть заполнено правильно';
			}
		}

		return false;
	}

	/**
	 * проверка на ввод поля для поиска продукта
	 *
	 * @return string строка поля или пустая строка
	 */
	public function checkSearchField($name) {
		if (isset($_POST[$name])) {
			if ($_POST[$name] != '') {
				return $_POST[$name];
			}
		}

		return '';
	}

	/**
	 * установка типа отвека как успех
	 */
	public function success() {
		$this->result['type'] = 'success';
	}

	/**
	 * удаление в ответе ошибок
	 */
	public function deleteErrors() {
		unset($this->result['errors']);
	}

	/**
	 * вывод JSON ответа на AJAX запросы
	 *
	 * @return string строка JSON ответа
	 */
	public function response() {
		if (count($this->result) == 0) {
			$this->result = [
				'type' => 'error',
				'text' => 'Произошла ошибка. Обновите страницу и попробуйте снова',
			];
		}

		return json_encode($this->result);
	}
}

?>