<?php

/**
 * Класс пользователя
 */

class User {
	public $id;
	public $login;
	public $password;
	public $id_role;
	public $date_enter;
	public $hash;

	/**
	 * установка пользователя
	 *
	 * @param array $user массив пользователя
	 */
	public function setUser($user) {
		$this->id = (isset($user['id'])) ? $user['id'] : 0;
		$this->login = (isset($user['login'])) ? $user['login'] : '';
		$this->password = (isset($user['password'])) ? $user['password'] : '';
		$this->id_role = (isset($user['id_role'])) ? $user['id_role'] : 0;
		$this->date_enter = (isset($user['date_enter'])) ? $user['date_enter'] : '';
		$this->hash = (isset($user['hash'])) ? $user['hash'] : '';
	}

	/**
	 * выборка пользователя по логину
	 *
	 * @param string $login логин пользователя
	 * @return boolean 0 - не найден, 1 - найден
	 */
	public function getByLogin($login) {
		DB::getInstance();
		$user = DB::getUserByLogin($login);

		if (count($user) > 0) {
			$this->setUser($user);

			return true;
		}

		return false;
	}

	/**
	 * выборка пользователя по хэшу
	 *
	 * @param string $hash хэш пользователя
	 * @return boolean 0 - не найден, 1 - найден
	 */
	public function getByHash($hash) {
		DB::getInstance();
		$user = DB::getUserByHash($hash);

		if (count($user) > 0) {
			$this->setUser($user);

			return true;
		}

		return false;
	}

	/**
	 * установка даты входа пользоателя
	 */
	public function setDateEnter() {
		$this->date_enter = date('Y-m-d H:i:s');

		DB::getInstance();
		DB::setUserDateEnter($this->id, $this->date_enter);
	}

	public function setHash() {
		$this->hash = md5(time()).sha1(rand(-100500, 100500));

		DB::getInstance();
		DB::setUserHash($this->id, $this->hash);
	}
}

?>