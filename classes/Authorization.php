<?php

/**
 * Класс для проведения авторизации
 */

class Authorization {
	public $access_role = 1;

	/**
	 * установка входа пользователя по логину и паролю
	 *
	 * @param string $login логин пользователя
	 * @param string $password паролья пользователя
	 * @return boolean 0 - вход не удался, 1 - вход успешен
	 */
	public function setEnter($login, $password) {
		$user = new User();

		if ($user->getByLogin($login)) {
			$password = md5($password.crc32($login)).sha1($login.crc32($password));

			if ( ($password == $user->password) && ($user->id_role == $this->access_role) ) {
				if ($user->hash == '') {
					$user->setHash();
				}

				$_SESSION['hash_user'] = $user->hash;
				$user->setDateEnter();

				return true;
			}
		}

		return false;
	}

	/**
	 * проверка авторизован ли пользователь
	 *
	 * @return boolean 0 - не авторизован, 1 - авторизован
	 */
	public function checkAuthorize() {
		if ( (isset($_SESSION['hash_user'])) && ($_SESSION['hash_user'] != '') ) {
			$user = new User();

			if ( ($user->getByHash($_SESSION['hash_user'])) && ($user->id_role == $this->access_role) ) {
				$user->setDateEnter();

				return true;
			}
		}

		return false;
	}

	/**
	 * выход пользователя
	 */
	public function setExit() {
		if (isset($_SESSION['hash_user'])) {
			unset($_SESSION['hash_user']);
		}
	}
}

?>