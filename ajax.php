<?php

	require_once(__DIR__.'/settings.php');
	require_once(__DIR__.'/autoload.php');

	date_default_timezone_set('Europe/Samara');
	header("Content-type: text/html; charset=utf-8");
	session_start();

	$ajax = new Ajax();

	if ($ajax->checkRequest()) {
		switch ($ajax->action) {
			case 'authorization_enter':
				$ajax->setAuthorizationResponse();

				$login = $ajax->checkLogin();
				$password = $ajax->checkPassword();

				if ( ($login) && ($password) ) {
					$authorization = new Authorization();

					if ($authorization->setEnter($login, $password)) {
						$ajax->deleteErrors();
						$ajax->success();
					} else {
						$ajax->result['errors']['password'] = 'Пара логин-пароль не подходят';
					}
				}
			break;

			case 'authorization_check_authorize':
				$ajax->setTypeResponse();

				$authorization = new Authorization();

				if ($authorization->checkAuthorize()) {
					$ajax->success();
				}
			break;

			case 'authorization_exit':
				$authorization = new Authorization();

				$authorization->setExit();
				$ajax->success();
			break;

			case 'products_get_all':
				$authorization = new Authorization();

				if ($authorization->checkAuthorize()) {
					$ajax->setProductsResponse();

					$search = new Search();

					$ajax->result['products'] = $search->getAll();

					if (count($ajax->result['products']) > 0) {
						$ajax->success();
					}
				} else {
					$ajax->setTypeResponse();
				}
			break;

			case 'products_search':
				$authorization = new Authorization();

				if ($authorization->checkAuthorize()) {
					$ajax->setProductsResponse();

					$search = new Search();

					$category = $ajax->checkSearchField('category');
					$name = $ajax->checkSearchField('name');
					$provider = $ajax->checkSearchField('provider');

					$ajax->result['products'] = $search->getSearch($category, $name, $provider);
					$ajax->success();
				} else {
					$ajax->setTypeResponse();
				}
			break;
		}
	}

	echo $ajax->response();

?>