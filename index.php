<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="skype_toolbar" content="skype_toolbar_parser_compatible" />
	<meta http-equiv="pragma" content="no-cache"/>

	<link href="css/images/favicon.ico" sizes="32x32" rel="shortcut icon" type="image/x-icon" />
	<link href="css/images/favicon_48.ico" sizes="48x48" rel="shortcut icon" type="image/x-icon" />
	<link href="css/images/favicon_64.ico" sizes="64x64" rel="shortcut icon" type="image/x-icon" />

	<title>Кальяри - Цены поставщиков на продукты</title>

	<link rel='stylesheet' href='css/global.css'>

	<script defer src='js/bundle.js'></script>
</head>

<body>
</body>
</html>